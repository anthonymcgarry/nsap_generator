#!/usr/bin/env python3

# Call this class providing an Area ID and IP address to generate IS-IS NSAP address

class nsap_gen:

    def __init__(self, area_id=None, ip=None):
        self.area_id = area_id
        self.ip = ip

    # Method takes an Area ID and IP address
    def create_nsap(self, area_id, ip):

        # we split the IP into octets and put in a list
        ip_addr_split = ip.split(".")

        # Create an empty list that we will use to hold our octets
        # depending on the octet we may need to prefix some 0's
        result = []
        for val in ip_addr_split:
            # example .1. we want as 001
            if len(val) == 1:
                result.append("00" + val)
                # .10 as 010
            elif len(val) == 2:
                result.append("0" + val)
                # your getting my drift
            elif len(val) == 3:
                result.append(val)

        # Next we take our list of values ['010', '002', '030', '001'] and join them
        # 010002030001
        result = ''.join(result)

        # We specify the length of our result as a chunk = 12
        # We specify the size of the chunk sizes we want to break it into = 4 (type int. single / would give us a float)
        chunk, chunk_size = len(result), len(result) // 3

        # System ID portion of the NSAP as a list of 3 chunks. Splice result into 4 digit chucks
        # Step through the chunk sizes, for i = 0 then 4 then 8 (start, Stop, Step)
        # result is list of 0:0+4 = 0-3, then 4:4+4 = 4-7, finally 8-11
        sid = [result[i:i+chunk_size] for i in range(0, chunk, chunk_size)]

        # Now we join the four elements using a period to create our final NSAP address
        # 49.1111.0100.0203.0001.00
        nsap = "".join(["49.", area_id, ".%s" % sid[0], ".%s" % sid[1], ".%s" % sid[2], ".00"])
        return str(nsap)

