#!/usr/bin/env python3

"""
Take the value of an IP address or subnet and create the required IS-IS NSAP address(es)
NSAP Address "49.1111.0100.0203.0001.00" based on Area ID 1111 and IP 10.2.30.1 as the System ID
Static portion 49.area.aaab.bbcc.cddd.00

Run script supplying Area ID and IP Address or Subnet
Ex: python3 nasp_generator.py 1111 10.2.30.1
Ex: python3 nasp_generator.py 1111 10.2.30.0/24
"""


import argparse
import sys
import ipaddress

# import our nsap_gen class from the file nsap_gen_class in the lib directory
from lib.nsap_gen_class import nsap_gen


def main():

    try:

        parser = argparse.ArgumentParser()

        parser.add_argument("Area ID", help="Generate NSAP ", type=int, metavar="[0000-9999]")
        parser.add_argument("[IP Address or Subnet]", help="Generate NSAP Address from IP Address or a range from a Subnet", type=str)

        # If the Area ID is not equal to four character in length raise a NameError exception
        if len(str(sys.argv[1])) != 4:
            print('Please enter Area ID as four digits [0000-9999] !!')
            raise NameError

        # If the ip address or subnet is not formatted correctly raise the default exception
        ipaddress.ip_network(sys.argv[2])

    except NameError:

        error = sys.exc_info()[1]
        print("Please enter a valid Area ID.\n Example: 1111\n ", error)
        sys.exit(1)

    except:

        error = sys.exc_info()[1]
        print("Please enter a valid Area ID and IP address or Subnet Mask.\n Example: 1111 10.2.30.1\n ", error)
        sys.exit(1)

    # set the first argument to be variable area_id
    area_id = (sys.argv[1])
    
    # Initialize our nsap_gen class, make a new object based on our class and call it nsap
    nsap = nsap_gen()

    # ip address module handles the format of IP or subnet. For each IP or IP Subnet generate the required NSAP address
    for addr in ipaddress.ip_network(sys.argv[2]):

        # addr is of type <class 'ipaddress.IPv4Address'> (print(type(addr)) so we need to change to string
        # Then send to our class, we could convert on same line below
        addr = str(addr)
        nsap_addr = (nsap.create_nsap(area_id, addr))
        print(nsap_addr)


# https://stackoverflow.com/questions/419163/what-does-if-name-main-do
if __name__ == "__main__":
    main()
