Take the value of an IP address or subnet and create the required IS-IS NSAP address(es)
NSAP Address "49.1111.0100.0203.0001.00" based on Area ID 1111 and IP 10.2.30.1 as the System ID
Static portion 49.area.aaab.bbcc.cddd.00


Run script supplying Area ID and IP Address or Subnet
Ex: python3 nasp_generator.py 1111 10.2.30.1
Ex: python3 nasp_generator.py 1111 10.2.30.0/24